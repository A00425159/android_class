package com.example.danielarantes.myapplication;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.content.Context;
import java.util.Date;
import android.content.ContentValues;
import android.util.Log;

class InClassDatabaseHelper extends SQLiteOpenHelper {
    private static final String TAG = "InClassDatabaseHelper_DOA_";
    private static final String DB_NAME = "inclass";
    private static final int DB_VERSION = 1;
    public static final String TABLE_NAME = "PERSON";
    public static final String BMI_HISTORY = "BMI_HISTORY";

    public InClassDatabaseHelper(Context context){
        super(context,DB_NAME,null, DB_VERSION);
        Log.i(TAG, "create");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i(TAG, "onCreate");

        Log.i(TAG, "Creating PERSON table");
        // user table
        db.execSQL("CREATE TABLE "+TABLE_NAME+" ("
                + "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + "NAME TEXT, "
                + "PASSWORD TEXT, "
                + "HEALTH_CARD_NUMB TEXT, "
                + "DATE TEXT);");

        Log.i(TAG, "Creating BMI_HISTORY table");
        // bmi hist table
        db.execSQL("CREATE TABLE "+ BMI_HISTORY +" ("
                + "_id INTEGER, "
                + "DATE TEXT, "
                + "HEIGHT DOUBLE,"
                + "WEIGHT DOUBLE,"
                + "BMI DOUBLE);");

        Log.i(TAG, "Creating BMI_HISTORY key");
        db.execSQL("CREATE UNIQUE INDEX PK ON "+ BMI_HISTORY + "(_id, DATE);");

        Log.i(TAG, "onCreate exiting");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion == 1) {

        }

        if (oldVersion < 3){
            // code if DB is at version 1 or 2 ( version 1 will run both)

        }
    }
}