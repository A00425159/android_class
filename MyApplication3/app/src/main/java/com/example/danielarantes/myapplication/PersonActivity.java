package com.example.danielarantes.myapplication;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.database.sqlite.SQLiteDatabase;
import android.content.ContentValues;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.DateFormat;


public class PersonActivity extends AppCompatActivity {
    private Person person = null;
    private static final String TAG = "PersonActivity_DOA_";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person);
    }

    public void onClickEnter(View view){
        Log.i(TAG, "onClickEnter");
        InClassDatabaseHelper helper = new InClassDatabaseHelper(this);
        SQLiteDatabase db = helper.getWritableDatabase();

        // get the fields values and insert into the database
        String userName = ((EditText)findViewById(R.id.userName)).getText().toString();
        String userPass = ((EditText)findViewById(R.id.userPass)).getText().toString();
        long birthdate = (new Date()).getTime();
        String healthcard = ((EditText)findViewById(R.id.healthcard)).getText().toString();

        ContentValues personValues = new ContentValues();
        personValues.put("NAME", userName);
        personValues.put("PASSWORD", userPass);
        personValues.put("HEALTH_CARD_NUMB", healthcard);
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        personValues.put("DATE", dateFormat.format(new Date()));

        Log.i(TAG, "inserting Person data");
        long insertReturn = db.insert("PERSON", null, personValues);

        if(insertReturn == -1) {
            Log.e(TAG, "an error occurred when inserting");
            Log.e(TAG, "insertReturn was: " + insertReturn);
        }
        else {
            // goes to the bmi activity page
            Intent intent = new Intent(this, EnterBMIActivity.class);

            try (Cursor cursor = db.rawQuery("SELECT _ID FROM PERSON WHERE NAME = ?", new String[] {userName}, null)) {
                cursor.moveToNext();
                person = new Person(cursor.getInt(0), userName);
            }
            db.close();
            // sends the person to the next page
            intent.putExtra("Person", person);
            startActivity(intent);
        }
        db.close();
    }
}
