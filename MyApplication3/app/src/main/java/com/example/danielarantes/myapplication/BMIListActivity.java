package com.example.danielarantes.myapplication;

import android.app.ListActivity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.view.View;
import java.util.ArrayList;
import java.text.SimpleDateFormat;
import java.text.DateFormat;

public class BMIListActivity extends ListActivity {
    private java.util.List<BMIResult> results = new ArrayList<BMIResult>();
    private static final String TAG = "BMIListActivity_DOA_";
    private Person person = null;

    public BMIListActivity(){
        Log.i(TAG, "create");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");

        // gets the person from the login page.
        person = (Person)getIntent().getSerializableExtra("Person");

        InClassDatabaseHelper helper = new InClassDatabaseHelper(this);
        SQLiteDatabase db = helper.getReadableDatabase();
        Log.i(TAG, "before query");
        // gets the bmi history for this person
        try (Cursor cursor = db.rawQuery("SELECT DATE, HEIGHT, WEIGHT, BMI FROM BMI_HISTORY WHERE _ID = ?", new String[] {String.valueOf(person.getId())}, null)) {
            Log.i(TAG, "after query");
            if(cursor.getCount() > 0) {
                Log.i(TAG, "cursor count > 0");
                while (cursor.moveToNext()) {
                    Log.i(TAG, "move nextadb");
                    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

                    Log.i(TAG, cursor.getString(0));
                    SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                    results.add(new BMIResult(Double.parseDouble(cursor.getString(1)),
                            Double.parseDouble(cursor.getString(2)),
                            cursor.getString(0)));
                    Log.i(TAG, "date: " + cursor.getString(0));
                    Log.i(TAG, "height: " + cursor.getString(1));
                    Log.i(TAG, "weight: " + cursor.getString(2));
                    Log.i(TAG, "bmi: " + cursor.getString(3));
                    Log.i(TAG, "---------------------------------------");
                }
            }
        }

        ListView listBMIResults = getListView();

        // send it to a normal array (don't ask me...)
        BMIResult[] results2 = new BMIResult[results.size()];
        for(int i = 0; i < results.size(); i++){
            results2[i] = results.get(i);
        }

        ArrayAdapter<BMIResult> listAdapter = new ArrayAdapter<BMIResult>(
                this,
                android.R.layout.simple_list_item_1,
                results2
        );

        listBMIResults.setAdapter(listAdapter);
    }

    public void onListItemClick(ListView listView, View itemView, int position, long id){
        Log.i(TAG, "Clicked on " + results.get(position).toString());
    }
}
