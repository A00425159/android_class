package com.example.danielarantes.myapplication;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.view.View;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.DateFormat;


public class EnterBMIActivity extends AppCompatActivity {
    private Person person = null;
    private static final String TAG = "EnterBMIActivity_DOA_";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // gets the person from the login page.
        if(person == null) person = (Person)getIntent().getSerializableExtra("Person");
        Log.i(TAG, "person = " + person);
        Log.i(TAG, "ID = " + person.getId());
        Log.i(TAG, "NAME = " + person.getName());
        setContentView(R.layout.activity_enter_bmi);
    }

    public void calculate(View view){
        InClassDatabaseHelper helper = new InClassDatabaseHelper(this);
        SQLiteDatabase db = helper.getWritableDatabase();

        // gets the height
        EditText height = (EditText)findViewById(R.id.textHeight);
        String value = height.getText().toString();
        Double heightVal = Double.parseDouble(value);
        Log.i(TAG, "height: " + heightVal);

        EditText weight = (EditText)findViewById(R.id.textWeight);
        String valueW = weight.getText().toString();
        Double weightVal = Double.parseDouble(valueW);
        Log.i(TAG, "weight: " + weightVal);

        Double calc = (weightVal / (heightVal * heightVal));

        EditText result = (EditText) findViewById(R.id.textResult);
        result.setText(calc.toString());

        ContentValues bmi_history = new ContentValues();
        bmi_history.put("_ID", person.getId());
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        bmi_history.put("DATE", dateFormat.format(new Date()));
        bmi_history.put("HEIGHT", heightVal);
        bmi_history.put("WEIGHT", weightVal);
        bmi_history.put("BMI", calc);


        long insertReturn = db.insert("BMI_HISTORY", null, bmi_history);
        Log.i(TAG, "insertRet: " + insertReturn);
    }

    public void onClickViewHistory(View view){
        Log.i(TAG, "onClickViewHistory");
        Intent intent = new Intent(this, BMIListActivity.class);
        // sends the person to the next page
        intent.putExtra("Person", person);
        startActivity(intent);
    }
}
